%Instituto Superior Técnico
%Análise e Síntese de Algoritmos
%Projeto 2 - Grupo 51

# Introdução
Este projeto baseia-se na resolução de um problema que se resume em identificar, numa imagem, que pixéis fazem parte do primeiro plano ou do cenário e calcular o peso da sua segmentação mínima.

A nossa solução consiste na modelação dos dados de *input* num grafo dirigido no qual são adicionados dois vértices, o *source*, **s**, e o *target* ou *sink*, **t**. 
Cada vértice está ligado a **s** e a **t**, sendo a capacidade representada pelo valor preto e branco de cada vértice respetivamente.
Esta ligação verifica-se nos dois sentidos.

O nosso algoritmo baseia-se no de *Edmunds-Karp* para calcular o fluxo máximo, que representa o valor
da segmentação mínima, e as precedências de cada vértice de forma a decidir se representam um pixel
de primeiro plano ou cenário.

# Descrição da solução
A nossa solução pode ser resumida nas seguintes operações:

1. *Parse* dos valores de preto e branco de cada pixel para a lista de adjacências;

2. Cálculo dos caminhos de aumento de **s** a **t** e posterior armazenamento do seu valor;

3. *Parse* dos valores das conexões entre os vértices;

4. Execução do algoritmo *Edmunds-Karp* de forma a calcular o fluxo máximo do grafo;

5. *Print* para o *stdout* do fluxo máximo;

6. Consoante os valores de precedências de cada vértice *print* de "P" caso seja "null" ou "C"
   caso contrário.

A nossa lista de adjacências e composta por:
  1. Um *array* de vértices de tamanho n\*m;
  2. Cada vértice tem uma lista de arestas;
  3. Cada aresta tem um ponteiro:
    * Para o vértice que representa;
    * Para a aresta que faz o percurso inverso (i.e. Edge(u,v) tem um ponteiro para Edge(v,u));
    * Para a aresta seguinte.

# Análise Teórica
A complexidade do ponto:

1. *O*(*V*);

2. *O*(*V*);

3. *O*(*V*);

4. *O*(*VE²*);

5. *O*(*1*);

6. *O*(*V*);

Assim é possível concluir que a nossa solução escala com *O*(*VE²*).

# Avaliação Experimental
O gráfico da Figura 1 representa o tempo de execução da nossa solução em função do número de
vértices.

![y=tempo de execução; x=número de vértices](plot2.svg){width=100%}

Como é possível verificar pelo gráfico acima o algoritmo escala de forma quadrática em função do número de vértices.

Este resultado deve-se ao facto dos grafos analisados serem esparsos.
