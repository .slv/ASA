#!/bin/bash

[ -z $1 ] || [ -z $2 ] && echo "Error: Missing arguments" && echo "Usage ./generator <lines> <columns>" && exit

len="$(($1*$2))"
out=""

out=" $out$1 $2"$'\n\n'

for i in `seq 1 2`
do
  for i in `seq 1 $1`
  do
    for i in `seq 1 $2`
    do
      out="$out $(($RANDOM%9+1))"
    done
    out="$out"$'\n'
  done
  out="$out"$'\n'
done

for i in `seq 1 $(($len-$1))`
do
  out="$out $(($RANDOM%9+1))"
done

for i in `seq 1 $(($len-$2))`
do
  out="$out $(($RANDOM%9+1))"
done

echo "$out"
