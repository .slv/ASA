#include <stdio.h>
#include <stdlib.h>

typedef struct link link;
typedef struct node node;

struct node {
  int d;
  link* pi;
  link* head;
};

struct link {
  int capacity;
  link* reverse;
  node* node;
  link* next;
};

node **q, *adjList;

int main()
{
  int m, n, i, t, flow = 0;
  link* tmp;

  scanf("%d%d", &m, &n);
  t = m * n;

  adjList = (node*)malloc(sizeof(node) * (t + 2));

  /* black*/
  for (i = 0; i < t; i++) {
    adjList[i].head = (link*)malloc(sizeof(link));
    adjList[i].head->reverse = (link*)malloc(sizeof(link));
    adjList[i].head->reverse->reverse = adjList[i].head;

    adjList[i].head->reverse->next = adjList[t].head;
    adjList[t].head = adjList[i].head->reverse;

    adjList[i].head->node = &adjList[t];
    adjList[t].head->node = &adjList[i];

    adjList[i].head->capacity = 0;
    scanf("%d", &(adjList[t].head->capacity));
  }

  /* white*/
  for (i = 0; i < t; i++) {
    tmp = (link*)malloc(sizeof(link));
    tmp->next = adjList[i].head;

    adjList[i].head = tmp;
    adjList[i].head->reverse = (link*)malloc(sizeof(link));
    adjList[i].head->reverse->reverse = adjList[i].head;

    adjList[i].head->reverse->next = adjList[t + 1].head;
    adjList[t + 1].head = adjList[i].head->reverse;

    adjList[i].head->node = &adjList[t + 1];
    adjList[t + 1].head->node = &adjList[i];

    adjList[t + 1].head->capacity = 0;
    scanf("%d", &(adjList[i].head->capacity));


     int min = adjList[i].head->capacity < adjList[i].head->next->reverse->capacity ? adjList[i].head->capacity : adjList[i].head->next->reverse->capacity;

    adjList[i].head->capacity -= min;
    adjList[i].head->reverse->capacity += min;

    adjList[i].head->next->capacity += min;
    adjList[i].head->next->reverse->capacity -= min;
    flow += min;
  }

  /* horizontal*/
  for (i = 0; i < t; i++) {
    if (!(i%n == n - 1)) {
      tmp = (link*)malloc(sizeof(link));
      tmp->next = adjList[i].head;

      adjList[i].head = tmp;
      adjList[i].head->reverse = (link*)malloc(sizeof(link));
      adjList[i].head->reverse->reverse = adjList[i].head;

      adjList[i].head->reverse->next = adjList[i + 1].head;
      adjList[i + 1].head = adjList[i].head->reverse;

      adjList[i].head->node = &adjList[i + 1];
      adjList[i + 1].head->node = &adjList[i];

      scanf("%d", &(adjList[i].head->capacity));
      adjList[i + 1].head->capacity = adjList[i].head->capacity;
    }
  }

  /* vertical*/
  for (i = 0; i < t - n; i++) {
    tmp = (link*)malloc(sizeof(link));
    tmp->next = adjList[i].head;

    adjList[i].head = tmp;
    adjList[i].head->reverse = (link*)malloc(sizeof(link));
    adjList[i].head->reverse->reverse = adjList[i].head;

    adjList[i].head->reverse->next = adjList[i + n].head;
    adjList[i + n].head = adjList[i].head->reverse;

    adjList[i].head->node = &adjList[i + n];
    adjList[i + n].head->node = &adjList[i];

    scanf("%d", &(adjList[i].head->capacity));
    adjList[i + n].head->capacity = adjList[i].head->capacity;
  }


  q = (node **)malloc(sizeof(node*)*(t+2));
  do {
    int qadd = 0, qrm = 0, df;

    /* reset nodes*/
    for (i = 0; i < t + 2; i++) {
      adjList[i].pi = NULL;
    }

    /* push*/
    q[qadd++] = &adjList[t];

    link* cur;
    while (qadd != qrm) {
      cur = q[qrm++]->head;
      while (cur) {
        if (!cur->node->pi && cur->node!= &adjList[t] && cur->capacity > 0) {
          cur->node->pi = cur;
          q[qadd++] = cur->node;
        }
        cur = cur->next;
      }
    }

    if (adjList[t+1].pi) {
      df = -1;
      for (cur = adjList[t+1].pi; cur; cur = cur->reverse->node->pi) {
        df = df == -1 ? cur->capacity : cur->capacity > df ? df : cur->capacity;
      }
      for (cur = adjList[t+1].pi; df > 0 && cur; cur = cur->reverse->node->pi) {
        cur->capacity -= df;
        cur->reverse->capacity += df;
      }
      flow += df;
    }
  } while (adjList[t+1].pi);
  
  printf("%d\n\n", flow);

  for (i = 0;  i < t; i++) {
    !adjList[i].pi ? printf("P") : printf("C");
    (i+1)%n ? printf(" ") : puts(" ");
  }

  return 0;
}
