#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BTWN(A, B, C) (B < A && A < C)
#define VERTEX(A) (A->real)
#define VERTEX_HIGH(A) ((A->real)->high)
#define VERTEX_LOW(A) ((A->real)->low)
#define VERTEX_ID(A) ((A->real)->id)
#define VERTEX_SCC_ID(A) ((A->real)->scc_id)
#define VERTEX_ON_STACK(A) ((A->real)->on_stack)
#define MIN(A, B) (A < B ? A : B)

typedef struct vertex *Vertex; 
typedef struct node *Node; 
typedef struct scc *Scc; 

struct node {
  Node next;
  Vertex real;
};

struct vertex {
  int id;
  int on_stack;
  int high;
  int low;
  int scc_id;
};

struct scc {
  int id;
  int mark;
  Scc next;
};

/*global variables*/
Node *graph;
Node stack = NULL;
int visited = 0;
int scc_numb = 0;

/*visita do Homem da selva*/
void tarjan_visit(Node u)
{
  VERTEX_HIGH(u) = VERTEX_LOW(u) = visited++;
  /*Push*/
  Node temp_u = malloc(sizeof(Node));
  VERTEX_ON_STACK(u) = 1;
  VERTEX(temp_u) = VERTEX(u);
  temp_u->next = stack;
  stack=temp_u;
  /* for each v in adjacentes*/
  Node v = u->next;
  while(v != NULL) {
    /*verifica se VERTEX(v) esta no stack*/
    if(VERTEX_HIGH(v) == -1 || VERTEX_ON_STACK(v)){
      if(VERTEX_HIGH(v) == -1)
        tarjan_visit(graph[VERTEX_ID(v)-1]);
      VERTEX_LOW(u) = MIN(VERTEX_LOW(u), VERTEX_LOW(v));
    }
    v=v->next;
  }
  /* if d[u] == low[u]*/
  if(VERTEX_HIGH(u) == VERTEX_LOW(u)) {
    scc_numb++;
    Vertex p = NULL;
    do {
      /*Pop*/
      v = stack;
      stack = v->next;
      VERTEX_ON_STACK(v) = 0;
      VERTEX_SCC_ID(v) = scc_numb;
      p = VERTEX(v);
      free(v);
    } while(VERTEX(u) != p);
  }
}

int main() 
{
  int N, M, u, v, i, j;
  /*Graph creation*/
  scanf("%d%d", &N, &M);
  graph = malloc(sizeof(Node)*N);
  for(i = 0; i < N; i++) {
    graph[i] = malloc(sizeof(Node));
    graph[i]->next = NULL;
    Vertex v = malloc(sizeof(Vertex));
    v->id = i+1;
    v->high = -1;
    v->low = -1;
    v->on_stack = 0;
    v->scc_id = -1;
    VERTEX(graph[i]) = v;
  }
  /*Graph parsing*/
  for(i = 0; i < M && scanf("%d%d", &u, &v) && BTWN(u, 0, N+1) && BTWN(u, 0, N+1); i++) {
    Node connect = malloc(sizeof(Node));
    VERTEX(connect) = VERTEX(graph[--v]);
    connect->next = graph[--u]->next;
    graph[u]->next = connect;
  }
  /*HOMEM DA SELVA*/
  for(i = 0; i < N; i++)
    if(VERTEX_HIGH(graph[i]) == -1){
      tarjan_visit(graph[i]);
    }

  int connection = 0;
  printf("%d\n", scc_numb);

  Scc sccList[scc_numb];
  for(i = 0; i < scc_numb; i++) {
    sccList[i] = NULL;
  }

  for(i = 0; i < N; i++) {
    if(!sccList[VERTEX_SCC_ID(graph[i])-1]) {
      sccList[VERTEX_SCC_ID(graph[i])-1] = (Scc)malloc(sizeof(struct scc));
      sccList[VERTEX_SCC_ID(graph[i])-1]->id = VERTEX_ID(graph[i]);
      sccList[VERTEX_SCC_ID(graph[i])-1]->next = NULL;
      sccList[VERTEX_SCC_ID(graph[i])-1]->mark = 0;
    }
  }

  for(i = 0, j = 0; i < N; i++) {
    Node probe = graph[i]->next;
    j = VERTEX_SCC_ID(graph[i]);
    while(probe) {
      if(VERTEX_SCC_ID(probe) != j) {
        Scc iter = sccList[j-1];
        while(iter->next && iter->next->id != VERTEX_SCC_ID(probe) && sccList[iter->next->id-1]->id < sccList[VERTEX_SCC_ID(probe)-1]->id) {
          iter = iter->next;
        }
        if(!iter->next) {
          iter->next = (Scc)malloc(sizeof(struct scc));
          iter->next->id = VERTEX_SCC_ID(probe);
          iter->next->next = NULL;
          connection++;
        } else if(iter->next->id != VERTEX_SCC_ID(probe)) {
          Scc tmp = iter->next;
          iter->next = (Scc)malloc(sizeof(struct scc));
          iter->next->id = VERTEX_SCC_ID(probe);
          iter->next->next = tmp;
          connection++;
        }
      }
      probe = probe->next;
    }
  }

  printf("%d\n", connection); 
  for (i = 0; i<N; i++) {
    int scc_id_vertex = VERTEX_SCC_ID(graph[i]);
    Scc probe = sccList[--scc_id_vertex];
    int fixed_probe = sccList[scc_id_vertex]->id;
    int mark = sccList[scc_id_vertex]->mark;
    while(probe->next!=NULL && !mark) {
      printf("%d %d\n",fixed_probe, sccList[(probe->next)->id-1]->id);
      probe = probe->next;
    }
    sccList[scc_id_vertex]->mark = 1;
  }
  return 0;
}







