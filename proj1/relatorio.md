%Instituto Superior Técnico
%Análise e Síntese de Algoritmos
%Projeto 1 - Grupo 51

# Introdução
Este projeto baseia-se na solução de um problema que se resume à detecção de componentes fortemente ligadas num grafo e conexões entre estas.

A nossa solução é uma implementação do algoritmo de Tarjan, com uma consequente armazenação as componentes fortemente ligadas (SCCs) numa matriz de adjacências.

# Descrição da solução
A nossa solução pode ser resumido nas seguintes operações:

1. *Parse* do *input*, armazenando numa lista de adjacências.

2. Execução do algoritmo de Tarjan nesse grafo de forma a obter as SCCs. À medida que vão sendo encontradas SCCs são dados aos vértices da mesma componente um identificador único que os identifica como parte da mesma componente;

3. Geração de um array de listas ligadas, onde são armazenadas as conexões entre SCCs. Cada índice do array representa uma SCC. Na cabeça de cada lista é armazenado o id do vértice de menor índice dessa mesma SCC. Nas posições seguintes da lista são armazenados os ids das SCCs com as quais esta tem conexão. Estes estão ordenados pelo id do vértice de menor valor, de forma crescente.

4. *Print* para o *stdout* do resultado.

# Análise Teórica
A complexidade do ponto:

1. O(V+E);

2. O(V+E);

3. O((V+E)S);

4. O(S);

(S = número de componentes fortemente ligadas).

Assim é possível concluir que a nossa solução escala com O((V+E)S).

# Avaliação Experimental
![y=tempo de execução; x=número de vértices](2.svg){width=70%}

Como é possível verificar pelo gráfico acima, a regressão linear é f(x)=0.02x-110.78, escalando o algoritmo de forma linear em função do número de vértices.
